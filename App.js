class App {
    constructor() {
        const emailInput = document.getElementById('email')
        const countrySelect = document.getElementById('country')
        const checkboxInput = document.getElementById('checkbox')
        const subscribeButton = document.getElementById('subscribeButton')
               
        this.email = emailInput.value
        this.country = countrySelect.value
        this.isChecked= false
    

        emailInput.addEventListener('change', event => {
            this.email= event.target.value
            this.handleFormChange()
        })
        
        countrySelect.addEventListener('change', event => {
            this.country = event.target.value
            this.handleFormChange()
        })
        
        checkboxInput.addEventListener('click', event => {
            this.isChecked = event.target.checked
            this.handleFormChange()
        })

        subscribeButton.addEventListener('click', event => {
            this.subscribe()
        })

      
    }
    handleFormChange() {
        if(this.isValidEmail() && this.isValidCountry() && this.isValidChecked()) {
            this.enableSubscribeButton()
        } else {
            this.disabledSubscribeButton()
        }
        
    }

    enableSubscribeButton() {
        const subscribeButton = document.getElementById('subscribeButton')
        subscribeButton.disabled = false
    }

    disabledSubscribeButton() {
        const subscribeButton = document.getElementById('subscribeButton')
        subscribeButton.disabled = true
    }

    isValidEmail() {
        const validFormat = /^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/
           return validFormat.test(this.email)
        }

    isValidCountry() {
        const validCountry = this.country
        if (validCountry === 'España' ||validCountry === 'Francia' || validCountry === 'Italia') {
            return true
            }
    }

    isValidChecked() {
        const checkboxInput = document.getElementById('checkbox')
        let isChecked = checkboxInput.checked
        return isChecked
    }

    subscribe() {
        alert('Perfecto te avisaremos la primera')
    }

}            
    
       






